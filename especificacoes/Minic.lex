package com.br.minic;

import com.br.minic.token.Token;
%%

%class AnalisadorLexico
%type Token
%line
%column

LETRA = [a-zA-Z]
DIGITO = [0-9]
IDENTIFICADOR = ({LETRA} | "_")({LETRA} | {DIGITO} | "_")*
LITERAL_INTEIRO = ({DIGITO})+
PONTO_FLUTUANTE = {DIGITO}+"."{DIGITO}+
LITERAIS_CARACTER = "'" ( "\\" ["t","n","r"] |{LETRA}) "'"
LITERAIS_STRING = "\"" [^\"]* "\""
COMENTARIO_LINHA = "//" [^\n]*
COMENTARIO_BLOCO = "/*" [^"/*"]* "*/"
FINAL_LINHA = (\r | \n | \r\n | \t)
ESPACO_BRANCO = [ ]
%%

"if" {return new Token("PR", yyline, yycolumn, yytext());}
"main" {return new Token("PR", yyline, yycolumn, yytext());}
"include" {return new Token("PR", yyline, yycolumn, yytext());}
"char" {return new Token("PR", yyline, yycolumn, yytext());}
"else" {return new Token("PR", yyline, yycolumn, yytext());}
"int" {return new Token("PR", yyline, yycolumn, yytext());}
"float" {return new Token("PR", yyline, yycolumn, yytext());}
"double" {return new Token("PR", yyline, yycolumn, yytext());}
"printf" {return new Token("PR", yyline, yycolumn, yytext());}
"printint" {return new Token("PR", yyline, yycolumn, yytext());}
"printstr" {return new Token("PR", yyline, yycolumn, yytext());}
"return" {return new Token("PR", yyline, yycolumn, yytext());}
"static" {return new Token("PR", yyline, yycolumn, yytext());}
"void" {return new Token("PR", yyline, yycolumn, yytext());}
"while" {return new Token("PR", yyline, yycolumn, yytext());}
"for" {return new Token("PR", yyline, yycolumn, yytext());}
"do" {return new Token("PR", yyline, yycolumn, yytext());}

"+" {return new Token("OA", yyline, yycolumn, yytext());}
"-" {return new Token("OA", yyline, yycolumn, yytext());}
"*" {return new Token("OA", yyline, yycolumn, yytext());}
"/" {return new Token("OA", yyline, yycolumn, yytext());}

"==" {return new Token("OC", yyline, yycolumn, yytext());}
"!=" {return new Token("OC", yyline, yycolumn, yytext());}
"<" {return new Token("OC", yyline, yycolumn, yytext());}
">" {return new Token("OC", yyline, yycolumn, yytext());}
"<=" {return new Token("OC", yyline, yycolumn, yytext());}
">=" {return new Token("OC", yyline, yycolumn, yytext());}

"&&" {return new Token("OL", yyline, yycolumn, yytext());}
"||" {return new Token("OL", yyline, yycolumn, yytext());}

"=" {return new Token("ATR", yyline, yycolumn, yytext());}
"!" {return new Token("ONG", yyline, yycolumn, yytext());}

":" {return new Token("SP", yyline, yycolumn, yytext());}
";" {return new Token("SP", yyline, yycolumn, yytext());}
"," {return new Token("SP", yyline, yycolumn, yytext());}
"(" {return new Token("SP", yyline, yycolumn, yytext());}
")" {return new Token("SP", yyline, yycolumn, yytext());}
"{" {return new Token("SP", yyline, yycolumn, yytext());}
"}" {return new Token("SP", yyline, yycolumn, yytext());}
"." {return new Token("SP", yyline, yycolumn, yytext());}
"#" {return new Token("SP", yyline, yycolumn, yytext());}
"&" {return new Token("SP", yyline, yycolumn, yytext());}

{IDENTIFICADOR} {return new Token("ID", yyline, yycolumn, yytext());}
{LITERAL_INTEIRO} {return new Token("LI", yyline, yycolumn, yytext());}
{PONTO_FLUTUANTE} {return new Token("PF", yyline, yycolumn, yytext());}
{LITERAIS_CARACTER} {return new Token("LC", yyline, yycolumn, yytext());}
{LITERAIS_STRING} {return new Token("LS", yyline, yycolumn, yytext());}
{COMENTARIO_LINHA} {}
{COMENTARIO_BLOCO} {}
{ESPACO_BRANCO} {}
{FINAL_LINHA} {}
. {return new Token("ERRO", yyline, yycolumn, yytext());}
