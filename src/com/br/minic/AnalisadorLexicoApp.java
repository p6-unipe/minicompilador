package com.br.minic;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.br.minic.token.Token;

public class AnalisadorLexicoApp {
	public static void main(String[] args) throws IOException {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader("programas/Teste1.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		AnalisadorLexico analisadorLexico = new AnalisadorLexico(fileReader);
		
		List<Token> tokens = new ArrayList<Token>();
		List<Token> tokensErros = new ArrayList<Token>();
		
		Token token = analisadorLexico.yylex();
		
		while(token != null) {
			if(token.getTipo().equalsIgnoreCase("ERRO")){
				tokensErros.add(token);
			}else {
				tokens.add(token);
			}
			
			token = analisadorLexico.yylex();
		}
		
		System.out.println("\n\n ------ Tokens ------\n");
		
		for (Token tokenAux : tokens) {
			System.out.println( "  "+ tokenAux);
		}
		
		System.out.println("\n Total de Tokens: "+ tokens.size());
		
		System.out.println("\n\n ------ Erro ------\n");
		
		for (Token tokenAux : tokensErros) {
			System.out.println( "  "+ tokenAux);
		}
		
		System.out.println("\n Total de Erros: "+ tokensErros.size());
	}
}
