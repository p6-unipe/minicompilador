package com.br.minic.token;

import java.io.File;

public class GeradorAnalisadorLexico {
	
	public static void main(String[] args) {
		
		File arquivoEspecificacao = new File("especificacoes/Minic.lex");
		jflex.Main.generate(arquivoEspecificacao);
	}
}
